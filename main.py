"""
Data Processing Application Framework is an Apache Spark application which provides:
    - Generate and Anonymize CSV file
    - Parse fixed width file
"""

import argparse
import logging

from pyspark.sql import SparkSession

from src.anonymizer.data_anonymizer import DataAnonymizer
from src.utilities.spark_factory import SparkFactory


def main():
    """ Entry point for the data processing application framework
       Arguments:
       Returns:
       """

    logger = logging.getLogger('data-processing-framework')
    arg_parser: argparse.ArgumentParser = argparse.ArgumentParser()
    arg_parser.add_argument('--file_delimiter',
                            help='Please provide file delimiter for csv file.')
    arg_parser.add_argument('--input_file_path',
                            help='Path to the input file which is required to be anonymized.'
                            )
    arg_parser.add_argument('--output_file_path',
                            help='Output path where anonymized file required to be written.'
                            )

    args: argparse.Namespace = arg_parser.parse_args()

    spark: SparkSession = SparkFactory.get_spark_session_instance(SparkFactory)
    data_anonymizer: DataAnonymizer = DataAnonymizer()
    data_anonymizer.anonymize_file(input_path=args.input_file_path,
                                   output_path=args.output_file_path,
                                   delimiter=args.file_delimiter,
                                   spark=spark)


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    main()
