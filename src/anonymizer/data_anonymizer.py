from pyspark.sql import DataFrame, SparkSession
from pyspark.sql import functions as F
from typing import List
from faker import Factory
from pyspark.sql.types import StringType, StructType

from src.anonymizer.anonymizer_base import AnonymizerBase
from src.utilities.spark_factory import SparkFactory


class DataAnonymizer(AnonymizerBase):

    def __init__(self):
        super().__init__()

    def fake_first_name(self) -> str:
        """
        Returns a random first name
        :return:
        """
        faker = Factory.create()
        return faker.first_name()

    def fake_last_name(self) -> str:
        """
        Returns random last name
        :return: str
        """
        faker = Factory.create()
        return faker.last_name()

    def fake_address(self) -> str:
        """
        Returns random address
        :return:
        """
        faker = Factory.create()
        return faker.address()

    def fake_word(self) -> str:
        """
        Returns random word
        :return: str
        """
        faker = Factory.create()
        return faker.word()

    def anonymize_data_frame(self, input_df: DataFrame, anonymize_col: List[str], schema: StructType) -> DataFrame:
        """
        Anonymizes the data in given data frame
        :param input_df: DataFrame
            Data which requires anonymization
        :param anonymize_col: List[str]
            List of columns to be anonymized
        :param schema: StructType
            Struct schema of csv file
        :return: DataFrame
        """
        self.__logger__.info('Running anonymize_data_frame.......')
        output_df: DataFrame = input_df
        col_list: List[str] = ['first_name', 'last_name', 'address']

        if schema is None:
            schema = input_df.schema
        try:
            for col_name in anonymize_col:
                if col_name not in schema.fieldNames():
                    raise Exception('Column not available in the input file: {col}'.format(col=col_name))

                if col_name in col_list:
                    func = F.udf(getattr(self, 'fake_{col}'.format(col=col_name)), StringType())
                else:
                    func = F.udf(getattr(self, 'fake_word', StringType()))

                output_df = output_df.withColumn(col_name + '_anonymized', func())
                output_df = output_df.drop(F.col(col_name))
                output_df = output_df.withColumnRenamed('{col}_anonymized'.format(col=col_name), col_name)
        except Exception as ex:
            raise ex

        return output_df.select(*schema.fieldNames())

    def anonymize_file(self, input_path: str, output_path: str, delimiter: str, spark: SparkSession,
                       schema: StructType = None):
        """
        Anonymizes the provided file
        :param input_path: str
            Path to input file
        :param output_path:
            Path to output folder
        :param delimiter:
            CSV delimiter
        :param spark:
            Spark Session
        :param schema:
            Input file schema
        :return:
        """
        self.__logger__.info('Running anonymize_file.......')
        input_df: DataFrame = SparkFactory.read_data(spark=spark,
                                                     path=input_path,
                                                     file_extension='csv',
                                                     schema=schema,
                                                     delimiter=delimiter
                                                     )
        if not input_df or len(input_df.head(1)) == 0:
            raise Exception('No records found in the input file: {path}'.format(path=input_path))

        input_df.cache()
        output_df: DataFrame = self.anonymize_data_frame(input_df=input_df,
                                                         anonymize_col=['first_name', 'last_name', 'address'],
                                                         schema=schema
                                                         )
        SparkFactory.write_data(write_df=output_df, path=output_path, output_type='csv')
        self.__logger__.info('Data anonymized successfully and output written to : {path}'.format(path=output_path))
