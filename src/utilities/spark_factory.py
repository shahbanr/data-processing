import logging

from pyspark import SparkConf
from pyspark.sql import SparkSession, DataFrame
from typing import List

from pyspark.sql.types import StructType

from src.utilities.csv_factory import CSVFactory

logger = logging.getLogger('data-processing-platform')


class SparkFactory:
    @staticmethod
    def get_spark_conf() -> SparkConf:
        """
        Provides default spark configuration
        :return: SparkConf
        """
        spark_conf = SparkConf()

        # Miscellaneous configuration
        spark_conf.set('spark.executor.heartbeatInterval', '60s')
        spark_conf.set('spark.network.timeout', '120s')
        spark_conf.set('spark.num.executors', '2')
        spark_conf.set('spark.executor.cores', '2')
        spark_conf.set('spark.executor.memory', '2g')
        spark_conf.set('spark.ui.showConsoleProgress', True)

        return spark_conf

    @staticmethod
    def get_spark_session_instance(self, spark_conf: SparkConf = None) -> SparkSession:
        """
        Creates a singleton spark session instance
        :param self:
        :param spark_conf: SparkConf
        :return: SparkSession
        """
        if spark_conf is None:
            spark_conf = self.get_spark_conf()  # fetch the default spark conf above

        logger.info('Spark config settings:')
        for key, value in spark_conf.getAll():
            logger.info(str(key) + ': ' + str(value))

        if 'spark_session_singleton_instance' not in globals():
            session: SparkSession = SparkSession.builder.config(conf=spark_conf).getOrCreate()
            globals()['spark_session_singleton_instance'] = session

        return globals()['spark_session_singleton_instance']

    @staticmethod
    def write_data(write_df: DataFrame, path: str, output_type: str) -> None:
        """
        Writes the given dataframe to output path
        :param write_df: DataFrame
            Spark DataFrame that needs to written to files
        :param path: str
            Path where output files will be written
        :param output_type: str
            Type of output file e.g. csv
        :return: None
        """
        try:
            if output_type == 'csv':
                write_df.coalesce(1).write.mode('overwrite').option('header', 'true').csv(path)
            else:
                logger.error('Not data write implementation found for given output type: {output_type}'.format(
                    output_type=output_type)
                )
        except Exception as ex:
            logger.error('Could not write data frame at: {path}'.format(path=path))
            raise ex

    @staticmethod
    def read_data(spark: SparkSession, path: str,
                  file_extension: str, delimiter: str = ',', schema: StructType = None) -> DataFrame:
        """
        Reads data into a spark data frame using provided file path(s).
        :param spark: SparkSession
            SparkSession that will be used to read files.
        :param path: str
            File path to read data from.
        :param schema: StructType
            schema for CSV file
        :param file_extension: str
            File type e.g. csv or parquet
        :param delimiter: str
            If files being read are csv then the delimiter being used in csv
        :return: DataFrame
            Spark data frame containing the records read from given file path(s)
        """
        logger.info("Reading files: {path}".format(path=path))

        df: DataFrame = None
        try:
            if file_extension == 'parquet':
                df = spark.read.parquet(*path)
            elif file_extension == 'csv':
                if CSVFactory.check_file_exists(path):
                    if schema is None:
                        df = spark.read \
                            .option('header', 'true') \
                            .option('inferSchema', 'true') \
                            .option('delimiter', delimiter) \
                            .csv(path)
                    else:
                        df = spark.read \
                            .option('header', 'true') \
                            .option('delimiter', delimiter) \
                            .schema(schema=schema) \
                            .csv(path)
                else:
                    logger.info('No file exists at path: {path}'.format(path=path))
                    return df

        except Exception as ex:
            logger.error('Could not read files from : {path}'.format(path=path))
            raise ex
        return df
