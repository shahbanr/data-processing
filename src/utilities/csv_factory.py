import os
from pyspark.sql.types import StructType
from pyspark.sql import DataFrame, SparkSession


class CSVFactory:

    @staticmethod
    def check_file_exists(path: str) -> bool:
        """
        Checks if the file exists at the given path in the system.
        :param path: str
        :return: bool
        """
        if os.path.exists(path):
            return True
        else:
            return False
