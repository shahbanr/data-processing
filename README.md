# Data Engineering Coding Challenges

### Data processing

- This framework takes the path to a CSV file as input and processes the file to anonymize certain columns
- The program takes following arguments as input parameter.
    - --input_file_path - Path to input csv file.
    - --output_file_path - Path to write the anonymized csv file
    - --file_delimiter - Delimiter for csv file
  
- Unit test cases can be executed using following pytest command in the home directory of project.
## Libraries Used

- Apache Spark 2.4.4
- Pytest

## How to Run the job.
- Run following commands in root directory of project. Require spark 2.4.4.
```shell
spark-submit --deploy-mode client --executor-memory 2g \
 --driver-memory 4g --master local[4] --num-executors 3 --executor-cores 2 main.py \
 --input_file_path /Users/sriaz/Source/data-processing/data/personal_data.csv \
 --output_file_path /Users/sriaz/Source/data-processing/data/output \
 --file_delimiter ',' 
```