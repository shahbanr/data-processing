import os
import pytest
from pyspark.sql import DataFrame

from pyspark.sql.types import StructType, StringType, StructField, DateType

from src.anonymizer.data_anonymizer import DataAnonymizer


class TestDataAnonymizer:

    def test_anonymize_file_valid_file(self, spark_session, base_setup):
        """
        Test the anonymization with a valid sample file. Except the file to be anonymized successfully
        :param spark_session:
            SparkSession fixture
        :param base_setup:
            Base setup fixture
        :return:
        """
        data_anonymizer: DataAnonymizer = DataAnonymizer()
        input_path: str = os.path.abspath('../tests/test_data/personal_info.csv')
        output_path: str = os.path.abspath('../tests/test_data_temp/personal_info/')
        delimiter: str = ','
        schema: StructType = StructType([
            StructField(name='first_name', dataType=StringType(), nullable=True),
            StructField(name='last_name', dataType=StringType(), nullable=True),
            StructField(name='address', dataType=StringType(), nullable=True),
            StructField(name='date_of_birth', dataType=DateType(), nullable=True)
        ]
        )
        data_anonymized = data_anonymizer.anonymize_file(input_path=input_path,
                                                         output_path=output_path,
                                                         delimiter=delimiter,
                                                         spark=spark_session,
                                                         schema=schema)

        assert data_anonymized is None

    def test_anonymize_file_without_schema(self, spark_session, base_setup):
        """
        Test the anonymization without explicitly providing the file schema. Except the file to be anonymized
        successfully
        :param spark_session:
            SparkSession fixture
        :param base_setup:
            Base setup fixture
        :return:
        """
        data_anonymizer: DataAnonymizer = DataAnonymizer()
        input_path: str = os.path.abspath('../tests/test_data/personal_info.csv')
        output_path: str = os.path.abspath('../tests/test_data_temp/personal_info/')
        delimiter: str = ','

        data_anonymized = data_anonymizer.anonymize_file(input_path=input_path,
                                                         output_path=output_path,
                                                         delimiter=delimiter,
                                                         spark=spark_session,
                                                         schema=None)

        assert data_anonymized is None

    def test_anonymize_file_missing_col(self, spark_session, base_setup):
        """
        Test the anonymization with a file which does not match with schema
        Expect the test to raise exception
        :param spark_session:
            SparkSession fixture
        :param base_setup:
            Base setup fixture
        :return:
        """
        data_anonymizer: DataAnonymizer = DataAnonymizer()
        input_path: str = os.path.abspath('../tests/test_data/personal_info.csv')
        output_path: str = os.path.abspath('../tests/test_data_temp/personal_info/')
        delimiter: str = ','
        schema: StructType = StructType([
            StructField(name='first_name', dataType=StringType(), nullable=True),
            StructField(name='middle_name', dataType=StringType(), nullable=True),
            StructField(name='address', dataType=StringType(), nullable=True),
            StructField(name='date_of_birth', dataType=DateType(), nullable=True)
        ]
        )

        with pytest.raises(Exception) as ex:
            data_anonymized = data_anonymizer.anonymize_file(input_path=input_path,
                                                             output_path=output_path,
                                                             delimiter=delimiter,
                                                             spark=spark_session,
                                                             schema=schema)

        assert str(ex.value) == 'Column not available in the input file: last_name'

    def test_anonymize_file_empty(self, spark_session, base_setup):
        """
        Test the anonymization by providing an empty file
        Expect the test to raise exception
        :param spark_session:
            SparkSession fixture
        :param base_setup:
            Base setup fixture
        :return:
        """
        data_anonymizer: DataAnonymizer = DataAnonymizer()
        input_path: str = os.path.abspath('../tests/test_data/personal_info_empty.csv')
        output_path: str = os.path.abspath('../tests/test_data_temp/personal_info_empty/')
        delimiter: str = ','
        schema: StructType = StructType([
            StructField(name='first_name', dataType=StringType(), nullable=True),
            StructField(name='last_name', dataType=StringType(), nullable=True),
            StructField(name='address', dataType=StringType(), nullable=True),
            StructField(name='date_of_birth', dataType=DateType(), nullable=True)
        ]
        )

        with pytest.raises(Exception) as ex:
            data_anonymized = data_anonymizer.anonymize_file(input_path=input_path,
                                                             output_path=output_path,
                                                             delimiter=delimiter,
                                                             spark=spark_session,
                                                             schema=schema)

        assert 'No records found in the input file:' in str(ex.value)

    def test_anonymize_data_frame_not_standard(self, spark_session, base_setup):
        """
        Test the file with fields other than first_name, last_name, address and date_of_birth
        Expect the file to be anonymized successfully
        :param spark_session:
            SparkSession fixture
        :param base_setup:
            Base setup fixture
        :return:
        """
        data_anonymizer: DataAnonymizer = DataAnonymizer()
        schema: StructType = StructType([
            StructField(name='first_name', dataType=StringType(), nullable=True),
            StructField(name='middle_name', dataType=StringType(), nullable=True),
            StructField(name='address', dataType=StringType(), nullable=True)
        ]
        )
        input_df: DataFrame = spark_session.createDataFrame(data=[
            ('James', 'S', "14 Street Addr"),
            ('Nathan', 'P', "10 Street Addr")
        ],
            schema=schema
        )

        output_df = data_anonymizer.anonymize_data_frame(input_df=input_df,
                                                         anonymize_col=['first_name', 'middle_name'],
                                                         schema=schema)

        assert output_df.count() == 2
