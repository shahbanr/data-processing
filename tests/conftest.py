import logging

import pytest
from pyspark.sql import SparkSession
from pyspark import SparkConf

from src.utilities.spark_factory import SparkFactory


@pytest.fixture(scope='session')
def spark_session():
    """
    Setup a pytest session wide Spark Session using this fixture
    :return:
    """
    spark_conf = SparkConf()

    spark_conf.set('spark.master', 'local[*]')
    spark_conf.set('spark.app.name', 'Data Processing Framework')

    # Miscellaneous configuration
    spark_conf.set('spark.executor.heartbeatInterval', '60s')
    spark_conf.set('spark.network.timeout', '120s')
    spark_conf.set('spark.ui.showConsoleProgress', True)
    spark_conf.set('spark.driver.cores', 1)
    spark_conf.set('spark.executor.cores', 1)
    spark_conf.set('spark.executor.memory', '4g')

    spark: SparkSession = SparkFactory().get_spark_session_instance(self=SparkFactory, spark_conf=spark_conf)
    yield spark


@pytest.fixture(scope='session')
def base_setup():
    """
    Setup pytest session wide logging configuration using this fixture
    :return:
    """
    logging.basicConfig(level=logging.INFO)
