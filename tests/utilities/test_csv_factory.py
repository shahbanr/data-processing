from pyspark.sql.types import StructType, StructField, StringType, TimestampType

from src.utilities.csv_factory import CSVFactory


class TestCSVFactory:

    def test_check_file_exists_valid(self):
        """
        Test check_file_exist function with a valid path
        :return:
        """
        file_path: str = '../tests/test_data/personal_info.csv'
        file_exists: bool = CSVFactory.check_file_exists(path=file_path)

        assert file_exists is True

    def test_check_file_exists_invalid(self):
        """
        Test check_file_exist function with an invalid path
        :return:
        """
        file_path: str = '../tests/test_data/personal_info_invalid.csv'
        file_exists: bool = CSVFactory.check_file_exists(path=file_path)

        assert file_exists is False
