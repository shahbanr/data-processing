from pyspark import SparkConf
from pyspark.sql import SparkSession

from src.utilities.spark_factory import SparkFactory


class TestSparkFactory:

    def test_spark_session_without_conf(self, base_setup):
        """
        Test spark_session build function without providing spark config
        Spark Session should use default config.
        :param base_setup:
        :return:
        """
        spark: SparkSession = SparkFactory().get_spark_session_instance(self=SparkFactory)

        assert spark is not None
        assert spark.conf.get('spark.executor.heartbeatInterval') == '60s'
        assert spark.conf.get('spark.network.timeout') == '120s'
        assert spark.conf.get('spark.ui.showConsoleProgress')

    def test_get_spark_conf(self, base_setup):
        """
        Test that get_spark_conf function returns a valid SparkConf
        :param base_setup:
        :return:
        """
        spark_conf = SparkFactory.get_spark_conf()
        assert spark_conf is not None
