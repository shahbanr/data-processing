import csv
from faker import Factory
import datetime
from typing import List


def data_generate(num_of_rec: int, header: List[str]):
    fake = Factory.create()
    with open("../../data/personal_data.csv", 'wt') as csvFile:
        writer = csv.DictWriter(csvFile, fieldnames=header)
        writer.writeheader()
        for i in range(num_of_rec):
            first_name = fake.first_name()
            last_name = fake.last_name()
            date_of_birth = fake.date(pattern="%Y-%m-%d", end_datetime=datetime.date(2000, 1, 1))
            address = fake.address()
            writer.writerow({
                "first_name": first_name,
                "last_name": last_name,
                "address": address,
                "date_of_birth": date_of_birth
            })


if __name__ == '__main__':
    records = 10000000
    headers = ["first_name", "last_name", "address", "date_of_birth"]
    data_generate(records, headers)
    print("CSV generation complete!")
