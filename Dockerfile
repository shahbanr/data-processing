FROM datamechanics/spark:2.4.7-latest

ENV PYSPARK_MAJOR_PYTHON_VERSON=3
WORKDIR /opt/application
COPY requirements.txt .
RUN pip3 install -r requirements.txt

COPY src/ src/
COPY main.py .